<?php
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }
  // add upgrade script
  $nb_version = (defined('MODULE_BARCODE_VERSION') ? MODULE_BARCODE_VERSION : 'new');
  $current_version = '3.0.2'; // change this each time a new version is ready to release
  
  while ($nb_version != $current_version) {
    switch($nb_version) {
    	// add case for each previous version
      case 'new':
	  case '1.0.0':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/nb/new.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/nb/new.php');
	    }
        $nb_version = '3.0.0';          
	    break;				
      case '3.0.0':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/nb/3_0_1.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/nb/3_0_1.php'); 
        }
        $nb_version = '3.0.1';          
        break;
      case '3.0.1':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/nb/3_0_2.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/nb/3_0_2.php'); 
        }
        $nb_version = '3.0.2';          
        break;        
      default:
        $nb_version = $current_version;
        // break all the loops
        break 2;       
  	}
  }