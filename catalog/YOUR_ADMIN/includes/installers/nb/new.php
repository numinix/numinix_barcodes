<?php

if (defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK')) {
    if (!$sniffer->field_exists(TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK, 'stock_barcode')) {
        $sql = "ALTER TABLE " . TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK. " ADD stock_barcode varchar(100) NULL DEFAULT NULL after stock_attributes";
        $result = $db->Execute($sql);
    }
}


if(!$sniffer->field_exists(TABLE_ORDERS, 'orders_barcode')) $db->Execute("ALTER TABLE " . TABLE_ORDERS . " ADD orders_barcode varchar(100) NULL default NULL;");

if(!$sniffer->field_exists(TABLE_PRODUCTS, 'products_barcode')) $db->Execute("ALTER TABLE " . TABLE_PRODUCTS . " ADD products_barcode varchar(100) NULL default NULL;");

if(defined('TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK')) {
    if (!$sniffer->field_exists(TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK, 'stock_barcode')){
	    $db->Execute("ALTER TABLE " . TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK . " ADD stock_barcode varchar(32)NULL DEFAULT NULL;");
    }
}

$configurationsinstalled = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_BARCODE_SWITCH'");
if($configurationsinstalled->fields['configuration_group_id']) {
    $configuration_group_id = $configurationsinstalled->fields['configuration_group_id'];
}

$configurationversion = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_BARCODE_VERSION' AND configuration_value='3.0.0'");
if(!$configurationversion->fields['configuration_group_id']) {
    $addversiontoinstall = 1;
}

$configurationgroupset = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Numinix Barcode Configuration'");
if($configurationgroupset->fields['configuration_group_id']) {
    $configurationgroupsetyes = 1;
}

if($configuration_group_id &&  $addversiontoinstall == 1){
    $db->Execute("REPLACE INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added) VALUES('Version', 'MODULE_BARCODE_VERSION', '3.0.0', 'Version Installed:', " . $configuration_group_id . ", 0, NOW(), NOW());");
}

if($configuration_group_id &&  $configurationgroupsetyes != 1){
    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Numinix Barcode Configuration', 'Set Numinix Barcode Configuration Options', " . $configuration_group_id . ", '1');");
}

//DO INSTALL
if(!$configuration_group_id){
    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, visible) VALUES ('Numinix Barcode Configuration', 'Set Numinix Barcode Configuration Options', '1');");

    $configuration_group_id = $db->Insert_ID();

    $db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");


    $db->Execute("REPLACE INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id , sort_order, date_added, use_function, set_function) VALUES('Barcode Switch', 'MODULE_BARCODE_SWITCH', 'false', 'Display Barcode on Admin Packing Slip/Invoice?', " . $configuration_group_id . ", 1, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),('Barcode Orders Switch', 'MODULE_BARCODE_ORDERS_SWITCH', 'false', 'Add a barcode to the HTML email and checkout success?', " . $configuration_group_id . ", 2, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),('Barcode Type', 'MODULE_BARCODE_TYPE', 'barcode', 'Barcode type (note: UPC and ISBN require a separate module?', " . $configuration_group_id . ", 3, NOW(), NULL, 'zen_cfg_select_option(array(\'barcode\', \'upc\', \'isbn\'),'),('Barcode Mode', 'MODULE_BARCODE_MODE', 'image', 'Barcode output mode:', " . $configuration_group_id . ", 4, NOW(), NULL, 'zen_cfg_select_option(array(\'image\', \'html\'),'),('Barcode Auto', 'MODULE_BARCODE_AUTO', 'true', 'Auto create barcodes?', " . $configuration_group_id . ", 5, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),('Barcode Orders Default', 'MODULE_BARCODE_ORDERS_DEFAULT', '100000000000', 'Enter the default barcode for orders:', " . $configuration_group_id . ", 6, NOW(), NULL, NULL),('Barcode Stock By Attributes', 'MODULE_BARCODE_STOCK_BY_ATTRIBUTES', 'false', 'Use stock by attributes (required to support attributes)?', " . $configuration_group_id . ", 7, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),('Barcode Orders Default', 'MODULE_BARCODE_ORDERS_DEFAULT', '100000000000', 'Enter the default barcode for orders:', " . $configuration_group_id . ", 6, NOW(), NULL, NULL),('Barcode Products Default', 'MODULE_BARCODE_PRODUCTS_DEFAULT', '123456000000', 'Enter the default barcode for products if not entering barcodes manually<br/>(Note: all undefined barcodes will be created automatically using this as the base barcode, add more 0s for more products):', " . $configuration_group_id . ", 8, NOW(), NULL, NULL),('Barcode Attributes Default', 'MODULE_BARCODE_ATTRIBUTES_DEFAULT', '987654000000', 'Enter the default barcode for attributes (mandatory - requires stock by attributes):', " . $configuration_group_id . ", 9, NOW(), NULL, NULL),('Barcode Scale', 'MODULE_BARCODE_SCALE', '1', 'What scale should the barcode be (larger number means larger scale)?', " . $configuration_group_id . ", 10, NOW(), NULL, 'zen_cfg_select_option(array(\'1\', \'2\', \'3\')');");

}

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configNuminixBarcode' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configNuminixBarcode')) {
  echo"YES";
    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_BARCODE_VERSION' LIMIT 1;");
    $configuration_group_id = $configuration->fields['configuration_group_id'];
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configNuminixBarcode',
                              'BOX_CONFIGURATION_NB', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Numinix Barcode Configuration menu.', 'success');
    }
  } 
}

$messageStack->add('Installed Numinix Barcode v3.0.0', 'success');
