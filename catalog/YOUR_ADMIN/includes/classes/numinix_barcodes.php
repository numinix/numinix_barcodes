<?php
/**
  * numinix_barcodes class
  *
  * @package classes
  * @copyright 2008 Numinix http://www.numinix.com
  * @author Numinix
  * $Id: numinix_barcodes.php 6 2008-11-14 00:59:57Z numinix $
  **/
 
  class numinix_barcodes {
    var $barcode_type = MODULE_BARCODE_TYPE;
    var $attributes_default = MODULE_BARCODE_ATTRIBUTES_DEFAULT;
    var $products_default = MODULE_BARCODE_PRODUCTS_DEFAULT;
    
    function barcodes($products_array) {
      // for products with attributes
      if ($this->is_attribute($products_array)) {
	  
        $products_attributes_array = $this->is_attribute($products_array);
        $stock_id = $this->get_stock_id_from_products_attributes_array($products_attributes_array);
        if ($stock_id) { 
	
          if ($this->get_stock_barcode_exists($stock_id)) {
            $barcode = $this->get_stock_barcode_exists($stock_id);
          } else {
            $barcode = $this->create_barcode_from_stock_id($stock_id);
            $this->store_attributes_barcode($stock_id, $barcode);
          }
        } else {
		if ($this->get_products_barcode_exists($products_array['id']) != false) {
         $barcode = $this->get_products_barcode_exists($products_array['id']);
		
        } else {
          $barcode = $this->create_barcode_from_products_id($products_array['id']);
		  
          $this->store_products_barcode($products_array['id'], $barcode);
		  
       }     
          
		if ($barcode != '') {

        return $barcode;
      } else {
        return false;
      }  
        }
      // for products without attributes
      } else {
	  
        if ($this->get_products_barcode_exists($products_array['id']) != false) {
          $barcode = $this->get_products_barcode_exists($products_array['id']);
		 
        } else {
		
          $barcode = $this->create_barcode_from_products_id($products_array['id']);
          $this->store_products_barcode($products_array['id'], $barcode);
		   
        }        
      
      if ($barcode != '') {
	
        return $barcode;
      } else {
        return false;
      }  
	  }
    }
    
    function get_products_barcode_exists($products_id) {
      global $db;
      $sql = "SELECT products_barcode FROM " . TABLE_PRODUCTS . "
              WHERE products_id = " . $products_id . "
              LIMIT 1";
      $products_barcode = $db->Execute($sql);
      $products_barcode = $products_barcode->fields['products_barcode'];
      if ($products_barcode == '') {
        if ($this->barcode_type == 'upc' || $this->barcode_type == 'isbn') {
          $sql = "SELECT products_upc, products_isbn FROM " . TABLE_PRODUCTS . "
                   WHERE products_id = " . $products_id . "
                   LIMIT 1";
          $products = $db->Execute($sql);
          if (($products_barcode = $products->fields['products_isbn']) == '')
          if (($products_barcode = $products->fields['products_upc']) == '')
            return false;
        } else {
          return false;
        }
      } else {
        return $products_barcode;
      }
    }
    
    // returns an array of attribute_ids for a product or false
    function is_attribute($products_array) {
      global $db;
      if (is_array($products_array['attributes'])) {
        $attributes_ids_array = array();
        foreach($products_array['attributes'] as $attribute) {
		if($attribute['product_attribute_is_free'] != 1){
         $options_id = $this->get_options_id_from_options_name($attribute['option']);
         $options_values_id = $this->get_options_values_id_from_options_values_name($attribute['value']);
        $attributes_id = $this->get_attributes_id_from_options_id_and_options_values_id($products_array['id'], $options_id, $options_values_id);
        $attributes_ids_array[] = $attributes_id; 
		
		}
        }
		if(is_array($attributes_ids_array)){
        $attributes_list = implode(',', $attributes_ids_array);
        $products_attributes_array = array('id' => $products_array['id'], 'attributes' => $attributes_list);
        return $products_attributes_array;
		}else{
		return false;
		}
      } else {
        return false;
      }
    }
    
    function get_stock_barcode_exists($stock_id) {
      global $db;
	  
	  if(defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK'))
	  $attributestableuse = TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK;
	  
	  if(defined('TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK'))
	  $attributestableuse = TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK;
	  
	  
      if ($stock_id) {
        $sql = "SELECT stock_barcode FROM " . $attributestableuse . "
                WHERE stock_id = " . $stock_id . "
                LIMIT 1";
        $stock_barcode = $db->Execute($sql);
        $stock_barcode = $stock_barcode->fields['stock_barcode'];
      }
      if ($stock_barcode == '') {
        return false;
      } else {
        return $stock_barcode;
      }
    }
    
    function store_attributes_barcode($stock_id, $stock_barcode) {
      global $db;
	  if(defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK'))
	  $attributestableuse = TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK;
	  
	  if(defined('TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK'))
	  $attributestableuse = TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK;
	  
      $sql = "UPDATE " . $attributestableuse . "
              SET stock_barcode = " . $stock_barcode . "
              WHERE stock_id = " . $stock_id . "
              LIMIT 1";
      $db->Execute($sql);
    }
    
    function store_products_barcode($products_id, $products_barcode) {
      global $db;
      $sql = "UPDATE " . TABLE_PRODUCTS . "
              SET products_barcode = " . $products_barcode . "
              WHERE products_id = " . $products_id . "
              LIMIT 1";
      $db->Execute($sql);
    }
    
    function create_barcode_from_products_id($products_id) {
      $barcode = $this->products_default + $products_id;
      return $barcode;
    }
    
    function create_barcode_from_stock_id($stock_id) {
      $barcode = $this->attributes_default + $stock_id;
      return $barcode;
    }
    
    function get_stock_id_from_products_attributes_array($products_attributes_array) {
      global $db;
      $products_id = $products_attributes_array['id'];
      $stock_attributes = $products_attributes_array['attributes'];
	if(defined('TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK') && $products_id && $stock_attributes){
	  $attributestableuse = TABLE_PRODUCTS_WITH_ATTRIBUTES_STOCK;
	  $sql = "SELECT stock_id FROM " . $attributestableuse . "
              WHERE products_id = " . $products_id . "
              AND stock_attributes = '" . $stock_attributes . "'
              LIMIT 1";
      $stock_id = $db->Execute($sql);
      $stock_id = $stock_id->fields['stock_id'];
	  }
	  
	  if(defined('TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_STOCK') && $stock_attributes){
	  $attributestableuse = TABLE_PRODUCTS_VARIANTS_ATTRIBUTES_GROUPS;
	  $sql = "SELECT stock_id FROM " . $attributestableuse . "
              WHERE products_attributes_id IN (" . $stock_attributes . ")
              LIMIT 1";
      $stock_id = $db->Execute($sql);
      $stock_id = $stock_id->fields['stock_id'];
	  }
        
      
      if ($stock_id != '') {
        return $stock_id;
      } else {
        return false;
      }
    } 

    function get_options_id_from_options_name($options_name) {
      global $db;
      $sql = "SELECT products_options_id FROM " . TABLE_PRODUCTS_OPTIONS . "
              WHERE products_options_name = '" . $options_name . "'
              LIMIT 1";
      $options_id = $db->Execute($sql);
      $options_id = $options_id->fields['products_options_id'];
      return $options_id;
    }
    
    function get_options_values_id_from_options_values_name($options_values_name) {
      global $db;
      $sql = "SELECT products_options_values_id FROM " . TABLE_PRODUCTS_OPTIONS_VALUES . "
              WHERE products_options_values_name = '" . $options_values_name . "'
              LIMIT 1";
      $options_values_id = $db->Execute($sql);
      $options_values_id = $options_values_id->fields['products_options_values_id'];
      return $options_values_id;
    }
    
    function get_attributes_id_from_options_id_and_options_values_id($products_id, $options_id, $options_values_id) {
      global $db;
      $sql = "SELECT products_attributes_id FROM " . TABLE_PRODUCTS_ATTRIBUTES . "
              WHERE products_id = " . $products_id . "
              AND options_id = " . $options_id . "
              AND options_values_id = " . $options_values_id . "
              LIMIT 1";
      $attributes_id = $db->Execute($sql);
      $attributes_id = $attributes_id->fields['products_attributes_id'];
      return $attributes_id;
    }
  }
?>